# Calculator

Calculator app for Android by following [the Udemy course](https://www.udemy.com/course/android-oreo-kotlin-app-masterclass/) of Tim Buchalka.

# Topics

1. __ConstraintLayout__ and grouping __Views__
2. General __onClickListener__ function for the calculator buttons
3. Function to perform calculation operations
4. __onSaveInstantState__ and __onRestoreInstantState__ functions to restore values after changing from portrait to landscape and visa versa

# Screen

<img src="screen_images/homescreen.jpeg" alt="drawing" width="220" />
