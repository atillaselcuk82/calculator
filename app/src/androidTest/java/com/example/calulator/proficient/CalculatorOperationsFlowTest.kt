package com.example.calulator.proficient

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.example.calulator.MainActivity
import com.example.calulator.R
import com.example.calulator.expert.Helper.assertResult
import com.example.calulator.expert.Helper.tap
import org.junit.Rule
import org.junit.Test

class CalculatorOperationsFlowTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    //Test 2 * 4 = 8.0
    fun testMultipy(){
        tap(R.id.button2)
        tap(R.id.buttonMultiply)
        tap(R.id.button4)
        tap(R.id.buttonEquals)
        assertResult("8.0")
    }

    @Test
    //Test 10 / 5 = 2.0
    fun testDivide(){
        tap(R.id.button1)
        tap(R.id.button0)
        tap(R.id.buttonDivide)
        tap(R.id.button5)
        tap(R.id.buttonEquals)
        assertResult("2.0")
    }

    @Test
    //Test 6 + 7 = 13.0
    fun testAdd(){
        tap(R.id.button6)
        tap(R.id.buttonPlus)
        tap(R.id.button7)
        tap(R.id.buttonEquals)
        assertResult("13.0")
    }

    @Test
    //Test 9 - 8 = 1.0
    fun testSubtract(){
        tap(R.id.button9)
        tap(R.id.buttonMinus)
        tap(R.id.button8)
        tap(R.id.buttonEquals)
        assertResult("1.0")
    }
}

