package com.example.calulator.basic

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.example.calulator.MainActivity
import com.example.calulator.R
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test

class CalculatorOperationsFlowTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    //Test 2 * 4 = 8.0
    fun testMultipy(){
        onView(withId(R.id.button2)).perform(click())
        onView(withId(R.id.buttonMultiply)).perform(click())
        onView(withId(R.id.button4)).perform(click())
        onView(withId(R.id.buttonEquals)).perform(click())
        onView(allOf(withId(R.id.result), withText("8.0"))).check(matches(isDisplayed()))
    }

    @Test
    //Test 10 / 5 = 2.0
    fun testDivide(){
        onView(withId(R.id.button1)).perform(click())
        onView(withId(R.id.button0)).perform(click())
        onView(withId(R.id.buttonDivide)).perform(click())
        onView(withId(R.id.button5)).perform(click())
        onView(withId(R.id.buttonEquals)).perform(click())
        onView(allOf(withId(R.id.result), withText("2.0"))).check(matches(isDisplayed()))
    }

    @Test
    //Test 6 + 7 = 13.0
    fun testAdd(){
        onView(withId(R.id.button6)).perform(click())
        onView(withId(R.id.buttonPlus)).perform(click())
        onView(withId(R.id.button7)).perform(click())
        onView(withId(R.id.buttonEquals)).perform(click())
        onView(allOf(withId(R.id.result), withText("13.0"))).check(matches(isDisplayed()))
    }

    @Test
    //Test 9 - 8 = 1.0
    fun testSubtract(){
        onView(withId(R.id.button9)).perform(click())
        onView(withId(R.id.buttonMinus)).perform(click())
        onView(withId(R.id.button8)).perform(click())
        onView(withId(R.id.buttonEquals)).perform(click())
        onView(allOf(withId(R.id.result), withText("1.0"))).check(matches(isDisplayed()))
    }
}

