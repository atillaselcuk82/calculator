package com.example.calulator.expert

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.example.calulator.R
import org.hamcrest.Matchers.allOf

object Helper {
    fun tap(id: Int) {
        onView(withId(id)).perform(click())
    }

    fun assertResult(text: String) {
        onView(allOf(withId(R.id.result), withText(text))).check(
            matches(isDisplayed())
        )
    }
}