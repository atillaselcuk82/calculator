package com.example.calulator.expert.robot

import com.example.calulator.R
import com.example.calulator.expert.Helper.assertResult
import com.example.calulator.expert.Helper.tap
import kotlinx.android.synthetic.main.activity_main.view.*

object calculatorScreen {

    fun tapZero() { tap(R.id.button0) }
    fun tapOne() { tap(R.id.button1) }
    fun tapTwo() { tap(R.id.button2) }
    fun tapThree() { tap(R.id.button3) }
    fun tapFour() { tap(R.id.button4) }
    fun tapFive() { tap(R.id.button5) }
    fun tapSix() { tap(R.id.button6) }
    fun tapSeven() { tap(R.id.button7) }
    fun tapEight() { tap(R.id.button8) }
    fun tapNine() { tap(R.id.button9) }

    fun tapMultiply() { tap(R.id.buttonMultiply) }
    fun tapDivide() { tap(R.id.buttonDivide) }
    fun tapAdd() { tap(R.id.buttonPlus) }
    fun tapSubtract() { tap(R.id.buttonMinus) }
    fun tapEquals() { tap(R.id.buttonEquals) }

    fun checkResultIs(text: String) { assertResult(text) }
}