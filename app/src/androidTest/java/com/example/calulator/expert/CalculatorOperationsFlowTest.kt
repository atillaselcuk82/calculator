package com.example.calulator.expert

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.example.calulator.MainActivity
import com.example.calulator.R
import com.example.calulator.expert.Helper.assertResult
import com.example.calulator.expert.Helper.tap
import com.example.calulator.expert.robot.calculatorScreen.checkResultIs
import com.example.calulator.expert.robot.calculatorScreen.tapAdd
import com.example.calulator.expert.robot.calculatorScreen.tapDivide
import com.example.calulator.expert.robot.calculatorScreen.tapEight
import com.example.calulator.expert.robot.calculatorScreen.tapEquals
import com.example.calulator.expert.robot.calculatorScreen.tapFive
import com.example.calulator.expert.robot.calculatorScreen.tapFour
import com.example.calulator.expert.robot.calculatorScreen.tapMultiply
import com.example.calulator.expert.robot.calculatorScreen.tapNine
import com.example.calulator.expert.robot.calculatorScreen.tapOne
import com.example.calulator.expert.robot.calculatorScreen.tapSeven
import com.example.calulator.expert.robot.calculatorScreen.tapSix
import com.example.calulator.expert.robot.calculatorScreen.tapSubtract
import com.example.calulator.expert.robot.calculatorScreen.tapTwo
import com.example.calulator.expert.robot.calculatorScreen.tapZero
import org.junit.Rule
import org.junit.Test

class CalculatorOperationsFlowTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    //Test 2 * 4 = 8.0
    fun testMultipy(){
        tapTwo()
        tapMultiply()
        tapFour()
        tapEquals()
        checkResultIs("8.0")
    }

    @Test
    //Test 10 / 5 = 2.0
    fun testDivide(){
        tapOne()
        tapZero()
        tapDivide()
        tapFive()
        tapEquals()
        checkResultIs("2.0")
    }

    @Test
    //Test 6 + 7 = 13.0
    fun testAdd(){
        tapSix()
        tapAdd()
        tapSeven()
        tapEquals()
        checkResultIs("13.0")
    }

    @Test
    //Test 9 - 8 = 1.0
    fun testSubtract(){
        tapNine()
        tapSubtract()
        tapEight()
        tapEquals()
        checkResultIs("1.0")
    }
}

